/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package prog.ud6.classwork.activitat617;

/**
 *
 * @author batoi
 */
public enum Tipo {
    TURISMO, 
    MONOVOLUMEN, 
    DEPORTIVO, 
    CROSSOVER, 
    TODO_TERRENO {
        @Override
        public String toString() {
            return "Todo terreno";
        }
    }
}
