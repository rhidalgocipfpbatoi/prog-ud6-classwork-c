/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat617;

/**
 *
 * @author batoi
 */
public class Coche {
    
    private String marca;
    private Tipo tipo;
    private Cambio cambio;

    public Coche(String marca, Tipo tipo, Cambio cambio) {
        this.marca = marca;
        this.tipo = tipo;
        this.cambio = cambio;
    }

    public Coche(String marca) {
        this(marca, Tipo.DEPORTIVO, Cambio.AUTOMATICO);
    }
    
    public Coche(Tipo tipo, String marca) {
        this.marca = marca;
        this.tipo = tipo;
        
        switch (tipo) {
            case DEPORTIVO:
                this.cambio = Cambio.AUTOMATICO;
            default:
                this.cambio = Cambio.MANUAL;
        }
   }

    @Override
    public String toString() {
        return "Coche{" + "marca=" + marca + ", tipo=" + tipo + ", cambio=" + cambio + '}';
    }
    
   
    
    
    
}
