/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat617;

/**
 *
 * @author batoi
 */
public class TestCoche {
    
    public static void main(String[] args) {
        Coche coche1 = new Coche("Seat", Tipo.MONOVOLUMEN, Cambio.MANUAL);
        Coche coche2 = new Coche("Porsche");
        Coche coche3 = new Coche(Tipo.TODO_TERRENO, "Land Rover");
        System.out.println(coche1);
        System.out.println(coche2);
        System.out.println(coche3);
        
    }
    
}
