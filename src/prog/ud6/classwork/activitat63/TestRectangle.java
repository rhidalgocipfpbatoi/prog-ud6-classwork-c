/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat63;

/**
 *
 * @author batoi
 */
public class TestRectangle {
    
    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(100, 200);
        System.out.println(r1.calcularArea());
        Rectangle r2 = new Rectangle(40, 100);
        Rectangle r3 = new Rectangle();
        Rectangle r4 = new Rectangle(56, 90);
        Rectangle r5 = new Rectangle(100, 150);
        Rectangle r6 = new Rectangle(120);
    }
    
}
