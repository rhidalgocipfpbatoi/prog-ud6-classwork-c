/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat63;

/**
 *
 * @author batoi
 */
public class Rectangle {
    
    // Atributos
    private int width;
    private int height;
    
    // Constructores
    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }
    
    public Rectangle(int height) {
        this(200, height);
    }
    
    public Rectangle() {
        this(80, 40);
    }
    
    // Métodos de objeto
    public int calcularArea() {
       return this.height * this.width;
    }
           
}
