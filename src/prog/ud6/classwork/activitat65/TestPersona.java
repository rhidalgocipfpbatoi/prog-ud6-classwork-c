/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat65;

/**
 *
 * @author batoi
 */
public class TestPersona {
    public static void main(String[] args) {
        Persona persona1 = new Persona("Pepe", "Pérez", 45);
        Persona persona2 = new Persona("Laura", "Gómez", 56, true);
        persona1.saluda();
        persona2.saluda();
    }
    
}
