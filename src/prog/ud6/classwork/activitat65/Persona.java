/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat65;

/**
 *
 * @author batoi
 */
public class Persona {
    
    private String nombre;
    private String apellidos;
    private int edad;
    private boolean estaCasado;

    public Persona(String nombre, String apellidos, int edad, boolean estaCasado) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.estaCasado = estaCasado;
    }

    public Persona(String nombre, String apellidos, int edad) {
        this(nombre, apellidos, edad, false);
    }
    
    public void saluda() {
        System.out.printf("Hola soy %s, tengo %d años y %s estoy casado%n",
                nombre.concat(" ").concat(apellidos),
                edad,
                estaCasado ?"sí":"no");
    }
    
}
