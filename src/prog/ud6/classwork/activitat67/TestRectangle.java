/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat67;

import prog.ud6.classwork.activitat66.*;
import java.util.Random;
import prog.ud6.classwork.activitat63.*;

/**
 *
 * @author batoi
 */
public class TestRectangle {
    
    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(100, 200);
        System.out.println(r1.calcularArea());
        
        Rectangle r2 = new Rectangle(40, 100);
        Random random = new Random();
        r2.setWidth(random.nextInt(190) + 11);
        r2.mostrarInfo();
        
        Rectangle r3 = new Rectangle();
        mostrarInfo(r3);
        
        Rectangle r4 = new Rectangle(56, 90);
        r4.mostrarInfo();
        
        
        Rectangle r5 = new Rectangle(100, 150);
        Rectangle r6 = new Rectangle(120);
        
        
    }
    
    public static void mostrarInfo(Rectangle rectangle) {
        System.out.println(rectangle.getHeight());
        System.out.println(rectangle.getWidth());
        System.out.println(rectangle.calcularArea());
    }
    
}
