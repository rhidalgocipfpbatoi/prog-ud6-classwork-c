/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat66;

import prog.ud6.classwork.activitat63.*;

/**
 *
 * @author batoi
 */
public class Rectangle {
    
    // Atributos
    private int width;
    private int height;
    
    // Constructores
    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Rectangle(int height) {
        this(200, height);
    }
    
    public Rectangle() {
        this(80, 40);
    }
    
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    
    
    // Métodos de objeto
    public int calcularArea() {
       return this.height * this.width;
    }
    
    public void mostrarInfo() {
        System.out.println("Alto " + getHeight());
        System.out.println("Ancho " + getWidth());
        System.out.println("Área " + calcularArea());
    }
           
}
