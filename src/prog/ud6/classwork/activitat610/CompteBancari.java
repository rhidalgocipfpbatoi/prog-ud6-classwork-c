/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat610;

/**
 *
 * @author batoi
 */
public class CompteBancari {
    private String nomClient;
    private String numCompte;
    private float tipusInteres;
    private float saldo;
    private boolean estaBloquejat;

    public CompteBancari(String nomClient, String numCompte, float tipusInteres, float saldo, boolean estaBloquejat) {
        this.nomClient = nomClient;
        this.numCompte = numCompte;
        this.tipusInteres = tipusInteres;
        this.saldo = saldo;
        this.estaBloquejat = estaBloquejat;
    }
    
    public CompteBancari(String nomClient, String numCompte) {
        this.nomClient = nomClient;
        this.numCompte = numCompte;
        this.tipusInteres = 0.1f;
        this.saldo = 0;
        this.estaBloquejat = false;
    }
    
    public boolean reintegra(float quantitat) {
        if (!this.estaBloquejat) {
            this.saldo = this.saldo - quantitat;
            return true;
        }
        return false;
    }
    
    public boolean ingressa(float quantitat) {
        if (!this.estaBloquejat) {
            this.saldo = this.saldo + quantitat;
            return true;
        }
        return false;
    }

    public boolean isEstaBloquejat() {
        return estaBloquejat;
    }    
    
    public String getNomClient() {
        return this.nomClient;
    }

    public String getNumCompte() {
        return numCompte;
    }

    public float getTipusInteres() {
        return tipusInteres;
    }

    public void setEstaBloquejat(boolean bloquejat) {
        this.estaBloquejat = bloquejat;
    }
    
    public void mostraInfo() {
       System.out.printf("Compte: %s, Client: %s, Interés: %.2f%%, Saldo: %.2f€\n",
               this.numCompte, this.nomClient, this.tipusInteres, this.saldo); 
    }
    
    public boolean transferirA(CompteBancari compteDesti, float quantitat) {
        if(this.isEstaBloquejat() || compteDesti.isEstaBloquejat()) {
            return false;
        }        
        this.saldo -= quantitat;
        compteDesti.saldo = compteDesti.saldo + quantitat;
        return true;
    }
    
    
}


