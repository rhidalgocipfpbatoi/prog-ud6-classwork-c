/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud6.classwork.activitat610;

/**
 *
 * @author batoi
 */
public class TestCompteBancari {

    public static void main(String[] args) {
        CompteBancari compte1 = new CompteBancari("Pepe Pérez", "00010021");
        compte1.ingressa(1000);
        compte1.reintegra(100);
        compte1.mostraInfo();

        CompteBancari compte2 = new CompteBancari("Juan Sánchez", "214521421", 0.5f, 30000, false);
        compte2.mostraInfo();
        compte2.setEstaBloquejat(false);
        if (compte2.reintegra(500)) {
            System.out.println("Reintegrament fet amb exit.");
        } else {
            System.out.println("No es posible fer reintegrament. Pareix que el compte está bloquejat");
        }
               
        // Añadido: transferir dinero de una cuenta a otra.
        System.out.println("Abans de transferencia...");
        compte1.mostraInfo();
        compte2.mostraInfo();
        
        if (compte1.transferirA(compte2, 100)) {
            System.out.println("Transferencia ok");
        }
        else {
            System.out.println("Error en transferencia");
        }
        
        System.out.println("Després de transferencia...");
        compte1.mostraInfo();
        compte2.mostraInfo();

        System.out.println("Adeu!");

    }
}
